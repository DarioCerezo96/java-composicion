package com.clearminds.maquina;

import com.clearminds.componentes.Celda;
import com.clearminds.componentes.Producto;

public class MaquinaDulces {
	private Celda celda1;
	private Celda celda2;
	private Celda celda3;
	private Celda celda4;
	private double saldo;
	
	
	public double getSaldo() {
		return saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}

	public void configurarMaquina(String cod1,String cod2,String cod3,String cod4){
		this.celda1=new Celda(cod1);
		this.celda2=new Celda(cod2);
		this.celda3=new Celda(cod3);
		this.celda4=new Celda(cod4);
	}
	
	public void mostrarConfiguracion(){
		System.out.println("Celda 1:"+this.celda1.getCodigo()+"\n"+
				"Celda 2:"+this.celda2.getCodigo()+"\n"+
				"Celda 3:"+this.celda3.getCodigo()+"\n"+
				"Celda 4:"+this.celda4.getCodigo());
	}
	
	public Celda buscarCelda(String codigo){
		if(this.celda1.getCodigo().equals(codigo)){
			return this.celda1;
		}else if(this.celda2.getCodigo().equals(codigo)){
			return this.celda2;
		}else if(this.celda3.getCodigo().equals(codigo)){
			return this.celda3;
		}else if(this.celda4.getCodigo().equals(codigo)){
			return this.celda4;
		}else{
			return null;	
		}
	}
	
	public void cargarProducto(Producto producto,String codCelda,int items){
		Celda celdaRecuperada = this.buscarCelda(codCelda);
		if(celdaRecuperada!=null){
			celdaRecuperada.ingresarProducto(producto,items);
		}else{
			System.out.print("Celda no encontrada");
		}
		
	}
	
	public void mostrarProductos(){
		if(this.celda1!=null){
			
		}
		System.out.println("Celda 1 "+"[Codigo:"+celda1.getCodigo()+" Stock:"+celda1.getStock()+" Nombre:"+celda1.getProducto().getNombre()+" Precio:"+celda1.getProducto().getPrecio());
		System.out.println("Celda 2 "+"[Codigo:"+celda2.getCodigo()+" Stock:"+celda2.getStock()+" Nombre:"+celda2.getProducto().getNombre()+" Precio:"+celda2.getProducto().getPrecio());
		System.out.println("Celda 3 "+"[Codigo:"+celda3.getCodigo()+" Stock:"+celda3.getStock()+" Nombre:"+celda3.getProducto().getNombre()+" Precio:"+celda3.getProducto().getPrecio());
		System.out.println("Celda 4 "+"[Codigo:"+celda4.getCodigo()+" Stock:"+celda4.getStock()+" Nombre:"+celda4.getProducto().getNombre()+" Precio:"+celda4.getProducto().getPrecio());
	}
	
	public Producto buscarProductoEnCelda(String codigo){
		if(this.celda1.getCodigo().equals(codigo)){
			return this.celda1.getProducto();
		}else if(this.celda2.getCodigo().equals(codigo)){
			return this.celda2.getProducto();
		}else if(this.celda3.getCodigo().equals(codigo)){
			return this.celda3.getProducto();
		}else if(this.celda4.getCodigo().equals(codigo)){
			return this.celda4.getProducto();
		}else{
			return null;	
		}
	}
	
	public double consultarPrecio(String codigo){
		if(this.celda1.getCodigo().equals(codigo)){
			return this.celda1.getProducto().getPrecio();
		}else if(this.celda2.getCodigo().equals(codigo)){
			return this.celda2.getProducto().getPrecio();
		}else if(this.celda3.getCodigo().equals(codigo)){
			return this.celda3.getProducto().getPrecio();
		}else if(this.celda4.getCodigo().equals(codigo)){
			return this.celda4.getProducto().getPrecio();
		}else{
			return 0;	
		}
	}
	
	public Celda buscarCeldaProducto(String codigo){
		if(this.celda1.getProducto().getCodigo().equals(codigo)){
			return this.celda1;
		}else if(this.celda2.getProducto().getCodigo().equals(codigo)){
			return this.celda2;
		}else if(this.celda3.getProducto().getCodigo().equals(codigo)){
			return this.celda3;
		}else if(this.celda4.getProducto().getCodigo().equals(codigo)){
			return this.celda4;
		}else{
			return null;	
		}
	}
	
	public void incrementarProductos(String codigo,int items){
		Celda celdaEncontrada = this.buscarCeldaProducto(codigo);
		if(celdaEncontrada!=null){
			celdaEncontrada.setStock(items);;
		}else{
			System.out.print("Celda no encontrada");
		}
	}
	
	public void vender(String codigo){
		Celda celdaEncontrada = this.buscarCelda(codigo);
		if(celdaEncontrada!=null){
			celdaEncontrada.setStock(celdaEncontrada.getStock()-1);
			this.setSaldo(this.getSaldo()+celdaEncontrada.getProducto().getPrecio());
		}else{
			System.out.print("Celda no encontrada");
		}
	}
	
	public double venderConCambio(String codigo,double valor){
		Celda celdaEncontrada = this.buscarCelda(codigo);
		if(celdaEncontrada!=null){
			celdaEncontrada.setStock(celdaEncontrada.getStock()-1);
			this.setSaldo(this.getSaldo()+celdaEncontrada.getProducto().getPrecio());
			return valor-celdaEncontrada.getProducto().getPrecio();
		}else{
			return 10;
		}
	}
	
}
